# Printer Examples

Collection of printer examples and notes. Please look under Notes for full list of printer example to see if the subject
you seek is covered.

# Table of Contents

[TOC]

# About

I have multiple notes I want to create here for printing. Printing in Mac isn't very well covered and I ended up spending
hours looking for simple answers. Looking at other peoples question its clear that there isn't enough information out
there.

## Things I like to cover

* Printer Basics
    * Some of basics covered in the guide from apple.
        * Printer Components (NSPrintOperation, NSPrintINfo, NSPrintPanelAccessorizing)
        * Single Page
        * Multi Page
        * Resize (Done)
        * Custom Pagination
* Printer Panel
    * Custom Page Attribute
    * Trigger Preview Updates

# Reference

## [Printing Programming Guide for Mac](https://developer.apple.com/library/mac/documentation/cocoa/conceptual/Printing/osxp_aboutprinting/osxp_aboutprt.html)

Probably the best place to get you started. You'll be able to figure out most of the basic things you need to do for
Mac printing.

## [TextEdit Example Application](https://developer.apple.com/library/mac/samplecode/TextEdit/Introduction/Intro.html)

If your using a document based application this might be a good example to take a look at.

# How to use (move to main readme)

Fork this repository and play around with the project that is in here and test out what your trying to do. It should
be easier then trying to create your own project to test things out. Then if you want to contribute back, add some note
and send me a pull request.

# License (move to main readme)

Apache License v2 for the most part and will be marked other wise. If your working for one of those crazy company that
blocks StackOverflow maybe this isn't the place for you because a lot of the implementation comes from there (though not
a direct copy and paste). I try to add reference inline to code to note any code idea that was borrowed, so please
follow the link for more information.


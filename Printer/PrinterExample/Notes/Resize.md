# Printing - Resizing Content to Fill

[TOC]

## About

This is an example for printing when using a non-document based application and when you need the view to resize the
view when the printer option changes (e.g. orientation).

## What We Are Building

We have been requested to create a 1 page report that can dynamically layout the content based on the print
orientation. The printed material has a different look then what is shown on screen.

In the portrait mode the 2 section must be stacked on top and in landscape mode the 2 section must be stacked side by
side.

## Implementation

Implementation is pretty straight forward.

* Create 2 views, one for displaying and another for printing.
* View for print: (IPEResizeExamplePrintView and IPEResizeExampleView.xib)
    * Resize the view to fit.
    * Rearrange the subviews to match orientation.
* View for display: (IPEResizeExampleView and IPEResizeExampleView.xib)
    * Resizable view for displaying.

## Setting up the Display View

Very simple view that lays out 2 subviews next to each other in 2 different out for portrait and landscape. The labels
at the bottom will update to show the size of the view and display portrait or landscape depending on what the UI think
is right.

We update the frame of both view inside the layout method so we can update the layout when our size change.

## Setting up the Print View

Print view is exactly the same view as the display view but extended to support the extra print functionality. We use
a different view since we need to resize and update the frame of this view.

### Updating the Width

When the print operation needs to adjust the width or the height, the operation will call
adjustPageWidthNew:left:right:limit: and/or adjustPageHeightNew:top:bottom:limit:.


## Displaying the Print Panel Dialog

Basic setup for showing the dialog. We will enable the orientation accessory view on the printer panel.

We will also need to make sure horizontal and vertical pagination is set to auto. This will change NSPrintOperation to
call a special method adjustPageHeightNew:top:bottom:limit: and adjustPageWidthNew:left:right:limit:, where you will
be able to resize your view accordingly.

```objective-c
// ask the view to give us a print version of it self.
NSView *viewToPrint = [self.view createPrintView];
NSPrintOperation *op = [NSPrintOperation printOperationWithView:viewToPrint];
NSPrintInfo *printInfo = [op printInfo];

NSPrintPanel *panel = [op printPanel];

// Enable page setup accessory view.
// Reference: http://stackoverflow.com/questions/8840678/how-do-i-get-page-attributes-option-in-cocoa-print-dialog
[panel setOptions:[panel options] | NSPrintPanelShowsPageSetupAccessory];

// Make sure we are using auto pagination for both horizontal and vertical. This is important, other wise the print
// operation will not call the width/height adjustment methods.
[printInfo setHorizontalPagination:NSAutoPagination];
[printInfo setVerticalPagination:NSAutoPagination];

// Display the print dialog
[op runOperation];
```

//
//  IPESubviewMaximizingView.m
//
// Created by Akito Nozaki on 2/8/14.
//


#import "IPESubviewMaximizingView.h"

@implementation IPESubviewMaximizingView

- (void)addSubview:(NSView *)view {
	// force auto sizing mask so we can resize this thing easier.
	view.translatesAutoresizingMaskIntoConstraints = YES;
	[view setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];

	view.frame = self.bounds;
	[super addSubview:view];
}


@end
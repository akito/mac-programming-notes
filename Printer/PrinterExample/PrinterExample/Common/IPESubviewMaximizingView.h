//
//  IPESubviewMaximizingView.h
//
// Created by Akito Nozaki on 2/8/14.
//



/**
 * @brief Simple view that will automatically adjust the superview to its bounds size.
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@interface IPESubviewMaximizingView : NSView
@end
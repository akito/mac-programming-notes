//
//  IPEColorView.h
//
// Created by Akito Nozaki on 2/8/14.
//



/**
 * @brief Simple view that can be colored
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@interface IPEColorView : NSView

@property(nonatomic) NSColor *backgroundColor;

@end
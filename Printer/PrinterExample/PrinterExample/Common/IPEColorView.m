//
//  IPEColorView.m
//
// Created by Akito Nozaki on 2/8/14.
//


#import "IPEColorView.h"

@implementation IPEColorView

- (void)setBackgroundColor:(NSColor *)backgroundColor {
	_backgroundColor = backgroundColor;
	[self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect {
	[self.backgroundColor setFill];
	NSRectFill(dirtyRect);
	[super drawRect:dirtyRect];
}


@end
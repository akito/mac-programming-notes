//
//  main.m
//  PrinterExample
//
//  Created by Akito Nozaki on 1/15/14.
//  Copyright (c) 2014 Ikazone. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}

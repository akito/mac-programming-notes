//
//  IPEExampleInfo.h
//
// Created by Akito Nozaki on 2/8/14.
//



/**
 * @brief Example's information
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@interface IPEExampleInfo : NSObject

+ (instancetype)infoWithTitle:(NSString *)title description:(NSString *)description exampleClass:(Class)exampleClass;

@property NSString *title;

@property NSString *description;

@property Class exampleClass;

- (instancetype)initWithTitle:(NSString *)title description:(NSString *)description exampleClass:(Class)exampleClass;

- (NSViewController *)viewControllerForExample;

@end
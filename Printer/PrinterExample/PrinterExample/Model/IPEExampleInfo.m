//
//  IPEExampleInfo.m
//
// Created by Akito Nozaki on 2/8/14.
//


#import "IPEExampleInfo.h"
#import "IPEResizeExampleViewController.h"

@implementation IPEExampleInfo


- (instancetype)initWithTitle:(NSString *)title description:(NSString *)description exampleClass:(Class)exampleClass {
	self = [super init];
	if (self) {
		self.title = title;
		self.description = description;
		self.exampleClass = exampleClass;
	}

	return self;
}

+ (instancetype)infoWithTitle:(NSString *)title description:(NSString *)description exampleClass:(Class)exampleClass {
	return [[self alloc] initWithTitle:title description:description exampleClass:exampleClass];
}

- (NSViewController *)viewControllerForExample {
	return [self.exampleClass viewController];
}

@end
//
//  IPEAppDelegate.m
//  PrinterExample
//
//  Created by Akito Nozaki on 1/15/14.
//  Copyright (c) 2014 Ikazone. All rights reserved.
//

#import "IPEAppDelegate.h"
#import "IPEExampleInfo.h"
#import "IPEResizeExampleViewController.h"
#import "IPEManualSplitViewController.h"
#import "IPEBorderExampleViewController.h"

@interface IPEAppDelegate () <NSUserInterfaceValidations>

@property(nonatomic) IBOutlet NSArrayController *exampleListController;

@property(nonatomic, weak) IBOutlet NSView *exampleView;

@end

@implementation IPEAppDelegate

- (void)dealloc {
	// Shouldn't really matter but should do this all the time.
	[self.exampleListController removeObserver:self forKeyPath:@"selectionIndex"];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// I'm using NSArrayController -> NSTableView's content instead of doing all the table stuff manually. This saves
	// a bunch of time in Mac development.

	// create a list of examples.
	self.exampleList = @[
			[IPEExampleInfo infoWithTitle:@"Print Resize Example" description:@"" exampleClass:[IPEResizeExampleViewController class]],
			[IPEExampleInfo infoWithTitle:@"Print Manual Page Example" description:@"" exampleClass:[IPEManualSplitViewController class]],
			[IPEExampleInfo infoWithTitle:@"Print Border With View" description:@"" exampleClass:[IPEBorderExampleViewController class]]
	];

	// we want to get notified when the selection changes. We can do this with KVO.
	[self.exampleListController addObserver:self forKeyPath:@"selectionIndexes" options:(NSKeyValueObservingOptionNew) context:nil];

	[self startController:self.exampleList[0]];
}

#pragma mark - Private Methods

- (void)startController:(IPEExampleInfo *)info {
	if (self.workingController) {
		[self.workingController.view removeFromSuperview];
	}

	self.workingController = [info viewControllerForExample];
	[self.exampleView addSubview:self.workingController.view];
}

#pragma mark - KVO operations

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqual:@"selectionIndexes"]) {
		// setup the selected item.
		NSArray *objects = [self.exampleListController selectedObjects];
		if ([objects count]) {
			// should be ok, we have the table set to single selection.
			IPEExampleInfo *info = objects[0];
			[self startController:info];
		}

	}
}

#pragma mark - Printing

/*
 * Lets turn the print operation off if the main view being attached does not have a print operation.
 */
- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem {
	SEL theAction = [anItem action];

	if (theAction == @selector(printOperation:)) {
		return [self.workingController.view respondsToSelector:@selector(printOperation:)];
	}

	return NO;
}

/*
 * We are pointing the print menu item to printOperation because the default print wants to print the window and it is
 * really hard to disable that. If you don't want to disable the default print behaviour you can user print: but make
 * sure you have the responder chain setup correctly.
 */
- (IBAction)printOperation:(id)sender {
	if ([self.workingController.view respondsToSelector:@selector(printOperation:)]) {
		[(id)self.workingController.view printOperation:sender];
	}
}

@end

//
//  IPEAppDelegate.h
//  PrinterExample
//
//  Created by Akito Nozaki on 1/15/14.
//  Copyright (c) 2014 Ikazone. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface IPEAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@property (nonatomic) NSArray *exampleList;

@property (nonatomic) NSViewController *workingController;

@end

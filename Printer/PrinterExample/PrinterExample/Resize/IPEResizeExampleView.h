//
//  IPEResizeExampleView.h
//
// Created by Akito Nozaki on 2/8/14.
//

#import "IPEResizeExampleViewProtocol.h"

@class IPEResizeExamplePrintView;

/**
 * @brief This is the view that will be displayed to the user.
 *
 * This is the page the user will see when printing. We will not be using CALayer for this view to make things easier
 * for this example.
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@interface IPEResizeExampleView : NSView <IPEResizeExampleViewProtocol>

@property(nonatomic, weak) IBOutlet id <IPEResizeExampleViewDelegate> delegate;

@property(nonatomic, weak) IBOutlet NSView *containerView;

@property(nonatomic, weak) IBOutlet NSTextField *widthTextField;

@property(nonatomic, weak) IBOutlet NSTextField *heightTextField;

@property(nonatomic, weak) IBOutlet NSTextField *displayTypeTextField;

@property(nonatomic, weak) IBOutlet IPEResizeExamplePrintView *printView;

@end
//
//  IPEResizeExampleViewProtocol.h
//
// Created by Akito Nozaki on 2/8/14.
//


@protocol IPEResizeExampleViewDelegate;

/**
 * @brief View protocol for resize example's view to farther decouple view and view controller.
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@protocol IPEResizeExampleViewProtocol <NSObject>

@property(nonatomic, weak) id <IPEResizeExampleViewDelegate> delegate;

/**
 * @brief view for printing.
 */
- (NSView *)printView;

@end
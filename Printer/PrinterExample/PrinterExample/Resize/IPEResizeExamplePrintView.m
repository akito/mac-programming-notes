//
//  IPEResizeExamplePrintView.m
//
// Created by Akito Nozaki on 2/8/14.
//

#import "IPEResizeExampleView.h"
#import "IPEResizeExamplePrintView.h"

@implementation IPEResizeExamplePrintView

- (void)adjustPageWidthNew:(CGFloat *)newRight left:(CGFloat)oldLeft right:(CGFloat)oldRight limit:(CGFloat)rightLimit {
	[self adjustPageSize];
	*newRight = NSWidth(self.frame);

	[super adjustPageWidthNew:newRight left:oldLeft right:oldRight limit:rightLimit];
}

- (void)adjustPageHeightNew:(CGFloat *)newBottom top:(CGFloat)oldTop bottom:(CGFloat)oldBottom limit:(CGFloat)bottomLimit {
	[self adjustPageSize];
	*newBottom = 0;

	[super adjustPageHeightNew:newBottom top:oldTop bottom:oldBottom limit:bottomLimit];
}

// This must be called withing the printing operation.
- (void)adjustPageSize {
	NSPrintInfo *pi = [[NSPrintOperation currentOperation] printInfo];

	// Calculate the page height in points
	NSSize paperSize = [pi paperSize];

	NSRect frame = self.frame;
	frame.size.height = paperSize.height - [pi topMargin] - [pi bottomMargin];
	frame.size.width = paperSize.width - [pi leftMargin] - [pi rightMargin];

	self.frame = frame;
	[self layoutSubtreeIfNeeded];
}


@end
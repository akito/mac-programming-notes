//
//  IPEResizeExamplePrintView.h
//
// Created by Akito Nozaki on 2/8/14.
//



/**
 * @brief printing version of the view
 *
 * This is the view that will actually get printed.
 *
 * I think it is actually a better idea to have a separate view to do the printing so you can freely resize the view
 * without effecting the current display.
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@interface IPEResizeExamplePrintView : IPEResizeExampleView
@end
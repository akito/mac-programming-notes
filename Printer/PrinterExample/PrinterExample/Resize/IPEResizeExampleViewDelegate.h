//
//  IPEResizeExampleViewDelegate.h
//
// Created by Akito Nozaki on 2/8/14.
//



/**
 * @brief View delegate for the resize example's main view.
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@protocol IPEResizeExampleViewDelegate <NSObject>

- (void)resizeExampleViewDelegateOnPrint:(id)sender;

@end
//
//  IPEResizeExampleViewController.h
//
// Created by Akito Nozaki on 2/8/14.
//

/**
 * @brief Demonstration on how a resizing printer should work.
 *
 * IPEResizeExampleView - view controlled by this controller.
 * IPEResizeExamplePrintView - view used for printing.
 * IPEResizeExampleViewDelegate - view delegate
 * IPEResizeExampleViewProtocol - view protocol used by view controller.
 *
 * Both view for displaying and printing is defined inside IPEResizeExampleView.xib
 *
 * @author Akito Nozaki 
 * @date 2/8/14 originally created date
 */
@interface IPEResizeExampleViewController : NSViewController

+ (IPEResizeExampleViewController *)viewController;
@end
//
//  IPEResizeExampleViewController.m
//
// Created by Akito Nozaki on 2/8/14.
//


#import "IPEResizeExampleViewController.h"
#import "IPEResizeExampleViewProtocol.h"
#import "IPEResizeExampleViewDelegate.h"
#import "IPEResizeExampleView.h"

@interface IPEResizeExampleViewController () <IPEResizeExampleViewDelegate>

@end

@implementation IPEResizeExampleViewController

+ (IPEResizeExampleViewController *)viewController {
	return [[IPEResizeExampleViewController alloc] initWithNibName:@"IPEResizeExampleView" bundle:nil];
}

- (void)awakeFromNib {
}

#pragma mark - Resize Example View Delegate

- (void)resizeExampleViewDelegateOnPrint:(id)sender {
	// ask the view to give us a print version of it self.
	id<IPEResizeExampleViewProtocol> viewToPrint = [(id) self.view printView];
	viewToPrint.delegate = self;

	NSPrintOperation *op = [NSPrintOperation printOperationWithView:(id) viewToPrint];
	NSPrintInfo *printInfo = [op printInfo];

	NSPrintPanel *panel = [op printPanel];

	// Enable page setup accessory view.
	// Reference: http://stackoverflow.com/questions/8840678/how-do-i-get-page-attributes-option-in-cocoa-print-dialog
	[panel setOptions:[panel options] | NSPrintPanelShowsPageSetupAccessory];

	// Make sure we are using auto pagination for both horizontal and vertical. This is important, other wise the print
	// operation will not call the width/height adjustment methods.
	[printInfo setHorizontalPagination:NSAutoPagination];
	[printInfo setVerticalPagination:NSAutoPagination];

	// Display the print dialog
	[op runOperation];
}



@end
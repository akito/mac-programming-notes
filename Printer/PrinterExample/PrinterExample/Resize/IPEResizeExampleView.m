//
//  IPEResizeExampleView.m
//
// Created by Akito Nozaki on 2/8/14.
//


#import "IPEResizeExampleView.h"
#import "IPEResizeExampleViewDelegate.h"
#import "IPEColorView.h"

@interface IPEResizeExampleView()

@end

@implementation IPEResizeExampleView

#pragma mark - Setup

- (void)awakeFromNib {
	// we won't worry about the frame here, and let layout deal with the frame (init will set the frame to zero).
	IPEColorView *firstView = [[IPEColorView alloc] init];
	firstView.backgroundColor = [NSColor redColor];

	IPEColorView *secondView = [[IPEColorView alloc] init];
	secondView.backgroundColor = [NSColor blueColor];

	// save it so we can move it around.
	self.containerView.subviews = @[firstView, secondView];

}

#pragma mark - Display

- (void)layout {
	[super layout];

	// we are just simply laying the view out horizontal if the frame is wider and vertical if it is taller. Resize the
	// window to see how it reacts.
	NSArray *views = self.containerView.subviews;
	if(NSWidth(self.frame) > NSHeight(self.frame)) {
		// left/right layout
		[views[0] setFrame:NSMakeRect(0, 0, NSWidth(self.containerView.frame) / 2, NSHeight(self.containerView.frame))];
		[views[1] setFrame:NSMakeRect(NSWidth(self.containerView.frame) / 2, 0, NSWidth(self.containerView.frame) / 2, NSHeight(self.containerView.frame))];

		[self.displayTypeTextField setStringValue:@"Landscape"];
	} else {
		// top/bottom layout
		[views[0] setFrame:NSMakeRect(0, 0, NSWidth(self.containerView.frame), NSHeight(self.containerView.frame) / 2)];
		[views[1] setFrame:NSMakeRect(0, NSHeight(self.containerView.frame) / 2, NSWidth(self.containerView.frame), NSHeight(self.containerView.frame) / 2)];

		[self.displayTypeTextField setStringValue:@"Portrait"];
	}

	[self.widthTextField setStringValue:[NSString stringWithFormat:@"%d", (int) NSWidth(self.frame)]];
	[self.heightTextField setStringValue:[NSString stringWithFormat:@"%d", (int) NSHeight(self.frame)]];
}


#pragma mark - Actions

/*
 * print menu item is set to user printOperation: instead of print: to disable the default print operation (printing
 * of window). However, this printOperation: is called from the AppDelegate and is not getting called by the responder
 * chain.
 */
- (IBAction)printOperation:(id)sender {
	[self.delegate resizeExampleViewDelegateOnPrint:self];
}

@end
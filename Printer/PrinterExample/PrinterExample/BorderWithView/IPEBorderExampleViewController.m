//
//  IPEBorderExampleViewController.m
//
//  Created by Akito Nozaki on 2/10/14.
//

#import "IPEBorderExampleViewController.h"
#import "IPEBorderExampleViewDelegate.h"
#import "IPEBorderExampleViewProtocol.h"
#import "IPEBorderExampleView.h"

@interface IPEBorderExampleViewController() <IPEBorderExampleViewDelegate>

@property (nonatomic, readonly) id<IPEBorderExampleViewProtocol> viewP;

@end

@implementation IPEBorderExampleViewController

@dynamic viewP;

+ (IPEBorderExampleViewController *)viewController {
	return [[IPEBorderExampleViewController alloc] initWithNibName:@"IPEBorderExampleView" bundle:nil];
}

- (id <IPEBorderExampleViewProtocol>)viewP {
	return (id <IPEBorderExampleViewProtocol>) self.view;
}


- (void)awakeFromNib {
}

#pragma mark - Resize Example View Delegate

- (void)borderExampleViewDelegateOnPrint:(id)sender {
	// ask the view to give us a print version of it self.
	id<IPEBorderExampleViewProtocol> viewToPrint = [self.viewP printView];
	viewToPrint.delegate = self;

	NSPrintOperation *op = [NSPrintOperation printOperationWithView:(id) viewToPrint];
	NSPrintInfo *printInfo = [op printInfo];

	NSPrintPanel *panel = [op printPanel];

	// Enable page setup accessory view.
	// Reference: http://stackoverflow.com/questions/8840678/how-do-i-get-page-attributes-option-in-cocoa-print-dialog
	[panel setOptions:[panel options] | NSPrintPanelShowsPageSetupAccessory];

	// Make sure we are using auto pagination for both horizontal and vertical. This is important, other wise the print
	// operation will not call the width/height adjustment methods.
	[printInfo setHorizontalPagination:NSAutoPagination];
	[printInfo setVerticalPagination:NSAutoPagination];

	// Display the print dialog
	[op runOperation];
}

@end
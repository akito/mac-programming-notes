//
//  IPEBorderExampleViewProtocol.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>

@protocol IPEBorderExampleViewDelegate;

/**
 * @brief TODO brief description
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@protocol IPEBorderExampleViewProtocol <NSObject>

@property(nonatomic, weak) id<IPEBorderExampleViewDelegate> delegate;

/**
 * @brief view for printing.
 */
- (id<IPEBorderExampleViewProtocol>)printView;

@end
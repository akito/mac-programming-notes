//
//  IPEBorderExampleViewController.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>

/**
 * @brief TODO brief description
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@interface IPEBorderExampleViewController : NSViewController
+ (IPEBorderExampleViewController *)viewController;
@end
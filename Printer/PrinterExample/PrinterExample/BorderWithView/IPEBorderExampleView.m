//
//  IPEBorderExampleView.m
//
//  Created by Akito Nozaki on 2/10/14.
//

#import "IPEBorderExampleView.h"
#import "IPEBorderExampleViewDelegate.h"
#import "IPEColorView.h"

@interface IPEBorderExampleView()

@property NSWindow *renderWindow;

@end


@implementation IPEBorderExampleView

#pragma mark - Setup

- (void)awakeFromNib {
	if(!self.containerView) {
		self.containerView = self;
	}

	// we won't worry about the frame here, and let layout deal with the frame (init will set the frame to zero).
	IPEColorView *firstView = [[IPEColorView alloc] init];
	firstView.backgroundColor = [NSColor redColor];

	IPEColorView *secondView = [[IPEColorView alloc] init];
	secondView.backgroundColor = [NSColor blueColor];

	// save it so we can move it around.
	self.containerView.subviews = @[firstView, secondView];

	// We need to create a rendering window in order to render the border's view. Note that we could also create this
	// in IB and just set the options correctly but I'm putting this here to make it easier to see what options are
	// needed.
	//
	// The only important option here are the backing and defer param.
	//
	_renderWindow = [[NSWindow alloc] initWithContentRect:NSMakeRect( 0, 0, 1000, 1000 ) styleMask:NSBorderlessWindowMask backing:NSBackingStoreNonretained defer:NO];
	[_renderWindow setOpaque:NO];
	[_renderWindow setBackgroundColor:[NSColor clearColor]];
	[_renderWindow.contentView addSubview:self.borderView];

}

#pragma mark - Display

- (void)layout {
	[super layout];

	// we are just simply laying the view out horizontal if the frame is wider and vertical if it is taller. Resize the
	// window to see how it reacts.
	NSArray *views = self.containerView.subviews;
	if(NSWidth(self.frame) > NSHeight(self.frame)) {
		// left/right layout
		[views[0] setFrame:NSMakeRect(0, 0, NSWidth(self.containerView.frame) / 2, NSHeight(self.containerView.frame))];
		[views[1] setFrame:NSMakeRect(NSWidth(self.containerView.frame) / 2, 0, NSWidth(self.containerView.frame) / 2, NSHeight(self.containerView.frame))];

		[self.displayTypeTextField setStringValue:@"Landscape"];
	} else {
		// top/bottom layout
		[views[0] setFrame:NSMakeRect(0, 0, NSWidth(self.containerView.frame), NSHeight(self.containerView.frame) / 2)];
		[views[1] setFrame:NSMakeRect(0, NSHeight(self.containerView.frame) / 2, NSWidth(self.containerView.frame), NSHeight(self.containerView.frame) / 2)];

		[self.displayTypeTextField setStringValue:@"Portrait"];
	}

	[self.widthTextField setStringValue:[NSString stringWithFormat:@"%d", (int) NSWidth(self.frame)]];
	[self.heightTextField setStringValue:[NSString stringWithFormat:@"%d", (int) NSHeight(self.frame)]];

	[super layout];
}


#pragma mark - Actions

/*
 * print menu item is set to user printOperation: instead of print: to disable the default print operation (printing
 * of window). However, this printOperation: is called from the AppDelegate and is not getting called by the responder
 * chain.
 */
- (IBAction)printOperation:(id)sender {
	[self.delegate borderExampleViewDelegateOnPrint:self];
}

#pragma mark - Printing

- (void)adjustPageWidthNew:(CGFloat *)newRight left:(CGFloat)oldLeft right:(CGFloat)oldRight limit:(CGFloat)rightLimit {
	[self adjustPageSize];
	*newRight = NSWidth(self.frame);

	[super adjustPageWidthNew:newRight left:oldLeft right:oldRight limit:rightLimit];
}

- (void)adjustPageHeightNew:(CGFloat *)newBottom top:(CGFloat)oldTop bottom:(CGFloat)oldBottom limit:(CGFloat)bottomLimit {
	[self adjustPageSize];
	*newBottom = 0;

	[super adjustPageHeightNew:newBottom top:oldTop bottom:oldBottom limit:bottomLimit];
}


- (void)drawPageBorderWithSize:(NSSize)borderSize {
	NSPrintOperation *op = [NSPrintOperation currentOperation];
	NSPrintInfo *printInfo = [op printInfo];

	[self.pageNumber setStringValue:[NSString stringWithFormat:@"%lu", [op currentPage]]];

	[_renderWindow setFrame:NSMakeRect(0, 0, borderSize.width, borderSize.height) display:NO];

	// we are accounting for the actual printable area and adjusting the border. If you want the content area you can
	// use paperSize - margin on both side.
	NSRect printableArea = [printInfo imageablePageBounds];
	[self.borderView setFrame:NSMakeRect([printInfo leftMargin], NSMinX(printableArea), borderSize.width - [printInfo leftMargin] - [printInfo rightMargin], NSHeight(printableArea))];
	[_renderWindow.contentView layoutSubtreeIfNeeded];

	// force the content to display
	[_renderWindow display];
}

// This must be called within the printing operation.
- (void)adjustPageSize {
	NSPrintInfo *pi = [[NSPrintOperation currentOperation] printInfo];

	// Calculate the page height in points
	NSSize paperSize = [pi paperSize];

	NSRect frame = self.frame;
	frame.size.height = paperSize.height - [pi topMargin] - [pi bottomMargin];
	frame.size.width = paperSize.width - [pi leftMargin] - [pi rightMargin];

	self.frame = frame;
	[self layoutSubtreeIfNeeded];
}
@end
//
//  IPEBorderExampleView.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>

@protocol IPEBorderExampleViewDelegate;

/**
 * @brief TODO brief description
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@interface IPEBorderExampleView : NSView

@property(nonatomic, weak) IBOutlet id <IPEBorderExampleViewDelegate> delegate;

@property(nonatomic, weak) IBOutlet NSView *containerView;

@property(nonatomic, weak) IBOutlet NSTextField *widthTextField;

@property(nonatomic, weak) IBOutlet NSTextField *heightTextField;

@property(nonatomic, weak) IBOutlet NSTextField *displayTypeTextField;

@property(nonatomic, weak) IBOutlet NSTextField *pageNumber;

@property(nonatomic, weak) IBOutlet NSView *borderView;

@property(nonatomic, weak) IBOutlet IPEBorderExampleView *printView;



@end
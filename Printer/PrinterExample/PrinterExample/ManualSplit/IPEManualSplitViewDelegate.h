//
//  IPEManualSplitViewDelegate.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>

/**
 * @brief TODO brief description
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@protocol IPEManualSplitViewDelegate <NSObject>

- (void)manualSplitViewDelegateOnPrint:(id)sender;

@end
//
//  IPEManualSplitView.m
//
//  Created by Akito Nozaki on 2/10/14.
//

#import "IPEManualSplitView.h"
#import "IPEManualSplitViewDelegate.h"


@implementation IPEManualSplitView

- (void)awakeFromNib {
	[self.subviews[0] setBackgroundColor:[NSColor redColor]];
	[self.subviews[1] setBackgroundColor:[NSColor greenColor]];
	[self.subviews[2] setBackgroundColor:[NSColor blueColor]];
	[self.subviews[3] setBackgroundColor:[NSColor purpleColor]];
}

/*
 * print menu item is set to use printOperation: instead of print: to disable the default print operation (printing
 * of window). However, this printOperation: is called from the AppDelegate and is not getting called by the responder
 * chain.
 */
- (IBAction)printOperation:(id)sender {
	[self.delegate manualSplitViewDelegateOnPrint:self];
}

#pragma mark - Manual Split View Protocol

- (id <IPEManualSplitViewProtocol>)printView {
	// since print isn't resizing the view this should be fine.
	return self;
}

#pragma mark - Printing

- (void)adjustPageWidthNew:(CGFloat *)newRight left:(CGFloat)oldLeft right:(CGFloat)oldRight limit:(CGFloat)rightLimit {
	NSLog(@"Note this method is not called!! adjustPageWidthNew:left:right:limit:");
	[super adjustPageWidthNew:newRight left:oldLeft right:oldRight limit:rightLimit];
}

- (void)adjustPageHeightNew:(CGFloat *)newBottom top:(CGFloat)oldTop bottom:(CGFloat)oldBottom limit:(CGFloat)bottomLimit {
	NSLog(@"Note this method is not called!! adjustPageHeightNew:top:bottom:limit:");
	[super adjustPageHeightNew:newBottom top:oldTop bottom:oldBottom limit:bottomLimit];
}

/**
 * Let PrintOperation know that we know better how to split this view up for printing.
 */
- (BOOL)knowsPageRange:(NSRangePointer)range {
	// we want to print each subviews per page.
	range->location = 1;
	range->length = [self.subviews count];
	return YES;
}

/**
 * Return the drawing rectangle for a particular page number. Also play around with how the print changes when the frame
 * is changed. You'll see that the colored box will only get printed if it is visible in the frame.
 *
 * You should resize the view to the desired size then feed it into the print operation. This might not be possible if
 * you are sharing the view for display and printing like in this example. If you need the entire view to be printed put
 * it in a scroll view and set the frame size so all the content fits.
 */
- (NSRect)rectForPage:(NSInteger)page {
	return [self.subviews[(NSUInteger) page - 1] frame];
}

@end
//
//  IPEManualSplitView.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>
#import "IPEManualSplitViewProtocol.h"

/**
 * @brief TODO brief description
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@interface IPEManualSplitView : NSView <IPEManualSplitViewProtocol>

@property(nonatomic, weak) IBOutlet id<IPEManualSplitViewDelegate> delegate;

@end
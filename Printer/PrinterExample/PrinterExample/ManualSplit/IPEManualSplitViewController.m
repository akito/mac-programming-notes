//
//  IPEManualSplitViewController.m
//
//  Created by Akito Nozaki on 2/10/14.
//

#import "IPEManualSplitViewController.h"
#import "IPEManualSplitViewProtocol.h"
#import "IPEManualSplitViewDelegate.h"

@interface IPEManualSplitViewController() <IPEManualSplitViewDelegate>

@property (nonatomic, readonly) id<IPEManualSplitViewProtocol> viewP;

@end

@implementation IPEManualSplitViewController

@dynamic viewP;

+ (IPEManualSplitViewController *)viewController {
	return [[IPEManualSplitViewController alloc] initWithNibName:@"IPEManualSplitView" bundle:nil];
}

- (id <IPEManualSplitViewProtocol>)viewP {
	return (id <IPEManualSplitViewProtocol>) self.view;
}


- (void)awakeFromNib {
}

#pragma mark - Resize Example View Delegate

- (void)manualSplitViewDelegateOnPrint:(id)sender {
	// ask the view to give us a print version of it self.
	id<IPEManualSplitViewProtocol> viewToPrint = [self.viewP printView];
	viewToPrint.delegate = self;

	NSPrintOperation *op = [NSPrintOperation printOperationWithView:(id) viewToPrint];
	NSPrintInfo *printInfo = [op printInfo];

	NSPrintPanel *panel = [op printPanel];

	// Enable page setup accessory view.
	// Reference: http://stackoverflow.com/questions/8840678/how-do-i-get-page-attributes-option-in-cocoa-print-dialog
	[panel setOptions:[panel options] | NSPrintPanelShowsPageSetupAccessory];

	// Make sure we are using clip pagination for both horizontal and vertical.
	[printInfo setHorizontalPagination:NSClipPagination];
	[printInfo setVerticalPagination:NSClipPagination];

	// Display the print dialog
	[op runOperation];
}

@end
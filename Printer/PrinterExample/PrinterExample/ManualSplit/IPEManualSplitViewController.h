//
//  IPEManualSplitViewController.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>

/**
 * @brief Manual Split Example Controller
 *
 * Manual split demonstrates how to split up the print page by providing a NSRect to the operation. Also note unlike the
 * Resize example, the print operation will not notify us about page size change.
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@interface IPEManualSplitViewController : NSViewController
+ (IPEManualSplitViewController *)viewController;
@end
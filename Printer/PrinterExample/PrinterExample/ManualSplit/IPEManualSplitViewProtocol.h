//
//  IPEManualSplitViewProtocol.h
//
//  Created by Akito Nozaki on 2/10/14.
//

#import <Foundation/Foundation.h>

@protocol IPEManualSplitViewDelegate;

/**
 * @brief TODO brief description
 *
 * @author Akito Nozaki
 * @date 2/10/14 created date
 */
@protocol IPEManualSplitViewProtocol <NSObject>

@property(nonatomic, weak) id<IPEManualSplitViewDelegate> delegate;

/**
 * @brief view for printing.
 */
- (id<IPEManualSplitViewProtocol>)printView;


@end